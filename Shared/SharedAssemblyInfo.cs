﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Heuvelman Sound & Vision B.V./EndemolShineGroup")]
[assembly: AssemblyProduct("Heartbeat")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("2.4.0.0")]
[assembly: AssemblyFileVersion("2.4.0.0")]
[assembly: NeutralResourcesLanguage("")]