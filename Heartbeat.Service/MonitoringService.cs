﻿using System.ServiceProcess;
using Heartbeat.Core.Constants;
using Heartbeat.Core.Notifications;
using Heartbeat.Core.Services;

namespace Heartbeat.Service
{
    public partial class MonitoringService : ServiceBase
    {
        private readonly Monitoring _monitoring;

        public MonitoringService()
        {
            InitializeComponent();
            _monitoring = new Monitoring();
        }

        protected override void OnStart(string[] args)
        {
            _monitoring.Start();
        }

        protected override void OnStop()
        {
            _monitoring.Stop();
            NotificationServer.Instance.Stop();
        }
    }
}