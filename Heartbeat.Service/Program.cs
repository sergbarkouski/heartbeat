﻿using System.ServiceProcess;
using System.Threading.Tasks;
using Heartbeat.Core.Constants;
using Heartbeat.Core.Notifications;
using Heartbeat.Core.Services;

namespace Heartbeat.Service
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            NotificationServer.Instantiate(HeartbeatConstants.NotificationPort);
            NotificationServer.Instance.Start();

            var networkService = new NetworkService();
            var isOnline = networkService.IsOnline(2);
            if (!isOnline)
                networkService.RestartNetworkInterfaces();

            isOnline = networkService.IsOnline(10);
            if (!isOnline)
                return;

            var updateService = new UpdateService();
            updateService.CheckForAppUpdates();
            updateService.CheckForSettingsUpdates();

            using (var service = new MonitoringService())
            {
                ServiceBase.Run(service);
            }
        }
    }
}