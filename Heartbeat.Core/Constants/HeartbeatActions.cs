﻿namespace Heartbeat.Core.Constants
{
    public static class HeartbeatActions
    {
        public const string Power = "power";

        public const string Update = "update";

        public const string Settings = "settings";
    }
}
