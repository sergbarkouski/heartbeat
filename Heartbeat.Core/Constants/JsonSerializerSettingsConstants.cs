﻿using Newtonsoft.Json;

namespace Heartbeat.Core.Constants
{
    public class JsonSerializerSettingsConstants
    {
        public static JsonSerializerSettings DefaultSettings => new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };
    }
}
