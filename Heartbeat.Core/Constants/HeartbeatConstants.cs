﻿namespace Heartbeat.Core.Constants
{
    public static class HeartbeatConstants
    {
        public static readonly string HeartbeatEventLogName = "HeartbeatEventLog";

        public static readonly string HeartbeatEventSourceName = "HeartbeatApp";

        public static readonly string HeartbeatServiceName = "Heartbeat";

        public static readonly int NotificationPort = 22222;

        public static readonly string UpdateFolder = "update";
    }
}
