﻿using System.Diagnostics;
using System.Security;
using Heartbeat.Core.Constants;
using Heartbeat.Core.Notifications;

namespace Heartbeat.Core.Helpers
{
    public class WindowsEventLogger
    {
        private static readonly EventLog EventLog;

        static WindowsEventLogger()
        {
            CreateEventSource();
            EventLog = new EventLog
            {
                Source = HeartbeatConstants.HeartbeatEventSourceName,
                Log = HeartbeatConstants.HeartbeatEventLogName
            };
        }

        public static void Log(Message message)
        {
            switch (message.Type)
            {
                case MessageType.Failure: Log(message.Text, EventLogEntryType.Error); break;
                case MessageType.Error: Log(message.Text, EventLogEntryType.Error); break;
                case MessageType.Action: Log(message.Text, EventLogEntryType.Information); break;
                case MessageType.Success: Log(message.Text, EventLogEntryType.Information); break;
                case MessageType.Info: Log(message.Text, EventLogEntryType.Information); break;
                default: Log(message.Text, EventLogEntryType.Information); break;
            }
        }

        private static void Log(string message, EventLogEntryType entryType)
        {
            EventLog.WriteEntry(message, entryType);
        }

        private static void CreateEventSource()
        {
            try
            {
                var sourceExists = EventLog.SourceExists(HeartbeatConstants.HeartbeatEventSourceName);
                var logName = EventLog.LogNameFromSourceName(HeartbeatConstants.HeartbeatEventSourceName, ".");
                var logExists = EventLog.Exists(logName);

                if (sourceExists && logExists)
                    return;

                if (sourceExists)
                    EventLog.DeleteEventSource(HeartbeatConstants.HeartbeatEventSourceName);

                if (logExists)
                    EventLog.Delete(HeartbeatConstants.HeartbeatEventLogName);

                EventLog.CreateEventSource(HeartbeatConstants.HeartbeatEventSourceName, HeartbeatConstants.HeartbeatEventLogName);
            }
            catch (SecurityException ex)
            {
                NotificationServer.Instance.SendImmediateToAll(ex);
            }
        }
    }
}
