﻿using System;
using Heartbeat.Core.Notifications;

namespace Heartbeat.Core.Helpers
{
    public static class ConsoleHelper
    {
        public static ConsoleColor ChooseColor(string type)
        {
            switch (type)
            {
                case MessageType.Failure: return ConsoleColor.Red;
                case MessageType.Error: return ConsoleColor.DarkRed;
                case MessageType.Action: return ConsoleColor.White;
                case MessageType.Success: return ConsoleColor.Green;
                case MessageType.Info: return ConsoleColor.Blue;
                default: return ConsoleColor.Black;
            }
        }
    }
}