﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Heartbeat.Core.Notifications;
using Heartbeat.Core.Services;

namespace Heartbeat.Core.Helpers
{
    public static class NetworkHelper
    {
        public static bool CheckForInternetConnection(int attempts)
        {
            for (var i = 0; i < attempts; i++)
            {
                try
                {
                    using (var client = new WebClient())
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
                catch
                {
                    Task.Delay(1000).Wait();
                }
            }

            return false;
        }

        public static void Scan()
        {
            try
            {
                var pingSender = new Ping();

                const int start = 1;
                const int stop = 255;

                var ip = SystemInfo.GetLocalIpAddress();
                var ipParts = ip.Split('.');
                var mask = $"{ipParts[0]}.{ipParts[1]}.{ipParts[2]}.";

                for (var i = start; i < stop; i++)
                {
                    var reply = pingSender.Send(mask + i, 10);

                    NotificationServer.Instance.SendImmediateToAll($"\rScanning network.....{Math.Round((double.Parse(i.ToString()) / stop) * 100)}%   ");
                }

                NotificationServer.Instance.SendImmediateToAll("\rScanning network.....DONE");
            }
            catch (Exception e)
            {
                NotificationServer.Instance.SendImmediateToAll(e.Message);
            }
        }
    }
}
