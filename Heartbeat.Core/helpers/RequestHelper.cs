﻿using System.Net;
using System.Text;

namespace Heartbeat.Core.Helpers
{
    internal static class RequestHelper
    {
        public static HttpWebRequest CreateGetRequest(string url, string getData = null)
        {
            var resultUrl = url;
            if (!string.IsNullOrWhiteSpace(getData))
            {
                resultUrl = $"{url}?{getData}";
            }

            var request = (HttpWebRequest)WebRequest.Create(resultUrl);

            request.Accept = "text/html, application/xhtml+xml, */*";
            request.Timeout = 30000;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";

            return request;
        }


        public static HttpWebRequest CreatePostRequest(string url, string postData)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var data = Encoding.ASCII.GetBytes(postData);

            request.Accept = "text/html, application/xhtml+xml, */*";
            request.Timeout = 30000;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            return request;
        }
    }
}
