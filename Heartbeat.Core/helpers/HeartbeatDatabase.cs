﻿using System;
using System.IO;
using System.Linq;
using Heartbeat.Core.Models;
using JsonFlatFileDataStore;

namespace Heartbeat.Core.Helpers
{
    public class HeartbeatDatabase
    {
        private readonly DataStore _db;
        private IDocumentCollection<ActionsQueue> col;

        public HeartbeatDatabase()
        {
            _db = new DataStore(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/hb.db.json"), reloadBeforeGetCollection: true);

            //Change status of all pending, after reboot everything should be done
            col = _db.GetCollection<ActionsQueue>();

            col.UpdateMany(e => e.status == "pending", new { status = "done", updated = DateTime.Now });
        }

        public void delete(string _queue_id)
        {
            col = _db.GetCollection<ActionsQueue>();
            col.DeleteMany(e => e.queue_id == _queue_id);
        }

        public string getLastAction(string _id)
        {
            try
            {
                col = _db.GetCollection<ActionsQueue>();
                var result = col.AsQueryable().Where(e => e.queue_id == _id);
                if (result.Any())
                {
                    return result.ElementAt(0).last_action;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public string getLastStatus(string _id)
        {
            try
            {
                col = _db.GetCollection<ActionsQueue>();
                var result = col.AsQueryable().Where(e => e.queue_id == _id);
                if (result.Any())
                {
                    return result.ElementAt(0).status;
                }

                return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public void status_update(string _queue_id, string _action, string _status)
        {
            col = _db.GetCollection<ActionsQueue>();
            var action = new ActionsQueue
            {
                queue_id = _queue_id,
                last_action = _action,
                status = _status,
                updated = DateTime.Now
            };

            //Check if exists
            var results = col.AsQueryable().Where(e => e.queue_id == action.queue_id);

            if (results.Any())
            {
                col.UpdateOne(e => e.queue_id == action.queue_id, action);
            }
            else
            {
                col.InsertOne(action);
            }
        }
    }
}
