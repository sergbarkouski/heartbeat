﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Heartbeat.Core.Helpers
{
    internal static class IpMacMappingHelper
    {
        private static List<IpAndMac> _list;

        public static string FindIpFromMacAddress(string macAddress)
        {
            InitializeGetIPsAndMac();
            var item = _list.SingleOrDefault(x => x.Mac.Replace("-", "").ToLower() == macAddress.Replace("-", "").ToLower());
            return item?.Ip;
        }

        public static string FindMacFromIpAddress(string ip)
        {
            InitializeGetIPsAndMac();
            var item = _list.SingleOrDefault(x => x.Ip == ip);
            return item?.Mac;
        }

        private static StreamReader ExecuteCommandLine(string file, string arguments = "")
        {
            var startInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                FileName = file,
                Arguments = arguments
            };

            var process = Process.Start(startInfo);

            return process.StandardOutput;
        }

        private static void InitializeGetIPsAndMac()
        {
            // if (list != null)
            // return;

            var arpStream = ExecuteCommandLine("arp", "-a");
            var result = new List<string>();
            while (!arpStream.EndOfStream)
            {
                var line = arpStream.ReadLine().Trim();
                result.Add(line);
            }

            _list = result.Where(x => !string.IsNullOrEmpty(x) && (x.Contains("dynamic") || x.Contains("static")))
                .Select(x =>
                {
                    string[] parts = x.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    return new IpAndMac { Ip = parts[0].Trim(), Mac = parts[1].Trim() };
                }).ToList();
        }

        private class IpAndMac
        {
            public string Ip { get; set; }

            public string Mac { get; set; }
        }
    }
}
