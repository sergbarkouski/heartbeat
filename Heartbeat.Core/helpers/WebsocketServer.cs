﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Heartbeat.Core.Services;
using WebSocketSharp;

namespace Heartbeat.Core.Helpers
{
    internal class WebsocketServer : IStartableInstance
    {
        private static WebSocket _ws;

        private static string _id;
        private static string _key;
        private static string _server;

        public event EventHandler<HBEventArguments> WSSMessage;
        public static HBEventArguments args = new HBEventArguments();

        private readonly CancellationTokenSource _cancellationTokenSource;

        private enum SslProtocols
        {
            Ssl2 = 12,
            Ssl3 = 48,
            Tls = 192,
            Tls11 = 768,
            Tls12 = 3072,
            Tls13 = 12288,
            All = Ssl2 | Ssl3 | Tls11 | Tls12 | Tls13 | Tls
        }

        public WebsocketServer(string server, string id, string key)
        {
            _id = id;
            _key = key;
            _server = server;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public void Start()
        {
            try
            {
                _ws = new WebSocket(_server);

                _ws.OnOpen += Ws_OnOpen;
                _ws.OnClose += Ws_OnClose;
                _ws.OnMessage += Ws_OnMessage;
                _ws.OnError += Ws_OnError;

                if (!_cancellationTokenSource.IsCancellationRequested)
                {
                    Connect();
                }
            }
            catch (Exception e)
            {
                var msg = $"{DateTime.Now} > Error connecting HeartBeat Server {e.StackTrace}, reconnecting in 10 seconds...";
                NotificationServer.Instance.SendImmediateToAll(Message.Create(msg, MessageType.Error));

                Task.Delay(10000, _cancellationTokenSource.Token).ContinueWith(t => Connect(), _cancellationTokenSource.Token);
            }
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();

            _ws.OnOpen -= Ws_OnOpen;
            _ws.OnClose -= Ws_OnClose;
            _ws.OnMessage -= Ws_OnMessage;
            _ws.OnError -= Ws_OnError;

            _ws.Close(CloseStatusCode.Normal);
        }

        public bool Send(string data)
        {
            try
            {
                _ws.Send(data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void Ws_OnError(object sender, ErrorEventArgs e)
        {
            NotificationServer.Instance.SendImmediateToAll(Message.Create($"{DateTime.Now} > Response from HeartBeat server {e.Message}", MessageType.Info));
        }

        private void Ws_OnMessage(object sender, MessageEventArgs e)
        {
            NotificationServer.Instance.SendImmediateToAll(Message.Create($"{DateTime.Now} > Response from HeartBeat server {e.Data}", MessageType.Info));
            args.message = e.Data;
            OnMessage(args);
        }

        private void Connect()
        {
            try
            {
                _ws.Connect();
            }
            catch (Exception ex)
            {
                NotificationServer.Instance.SendImmediateToAll(Message.Create($"{DateTime.Now} > No connection to the HeartBeat server", MessageType.Error));
                NotificationServer.Instance.SendImmediateToAll(ex);
            }
        }

        private static void Ws_OnOpen(object sender, EventArgs e)
        {
            try
            {
                NotificationServer.Instance.SendImmediateToAll(Message.Create($"{DateTime.Now} > Connected to HeartBeat server, send ID", MessageType.Info));

                var deviceInfo = SystemInfo.GetDeviceInfo();
                _ws.Send(WebSocketMessage.Create(_id, _key, deviceInfo).ToJson());
            }
            catch (Exception exception)
            {
                NotificationServer.Instance.SendImmediateToAll(Message.Create($"{DateTime.Now} > Not connected to the HeartBeat server {exception.Message}", MessageType.Error));
            }
        }

        private void Ws_OnClose(object sender, CloseEventArgs e)
        {
            var sslProtocolHack = (System.Security.Authentication.SslProtocols)(SslProtocols.All);
            if (e.Code == 1015 && _ws.SslConfiguration.EnabledSslProtocols != sslProtocolHack)
            {
                _ws.SslConfiguration.EnabledSslProtocols = sslProtocolHack;
                Connect();
            }

            NotificationServer.Instance.SendImmediateToAll(Message.Create($"{DateTime.Now} > Disconnected from HeartBeat server (Code: {e.Code},{e.Reason}), reconnecting in 10 seconds...", MessageType.Error));
            Task.Delay(10000).ContinueWith(t => Connect());
        }


        protected virtual void OnMessage(HBEventArguments args)
        {
            WSSMessage?.Invoke(this, args);
        }
    }
}
