﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;

namespace Heartbeat.Core.Notifications
{
    public delegate void ClientHandleMessageReceived(Message message);

    public class Client
    {
        public event ClientHandleMessageReceived OnMessageReceived;

        private TcpClient _tcpClient;
        private NetworkStream _clientStream;

        private bool _started;

        public void ConnectToServer(string ipAddress, int port)
        {
            _tcpClient = new TcpClient(ipAddress, port);
            _clientStream = _tcpClient.GetStream();

            var thread = new Thread(ListenForPackets);
            _started = true;
            thread.Start();
        }

        private void ListenForPackets()
        {
            while (_started)
            {
                Message message;
                try
                {
                    var streamReader = new BinaryReader(_clientStream);
                    var data = streamReader.ReadString();
                    message = JsonConvert.DeserializeObject<Message>(data);
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("A socket error has occurred with the client socket " + _tcpClient);
                    break;
                }

                if (message == null)
                    continue;

                OnMessageReceived?.Invoke(message);

                Thread.Sleep(15);
            }

            _started = false;
            Disconnect();
        }

        public bool IsConnected()
        {
            return _started && _tcpClient.Connected;
        }

        public void Disconnect()
        {
            if (_tcpClient == null)
            {
                return;
            }

            _tcpClient.Close();
            _tcpClient.Dispose();

            _started = false;
        }
    }
}