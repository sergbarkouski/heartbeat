﻿namespace Heartbeat.Core.Notifications
{
    public static class MessageType
    {
        public const string Error = "error";
        public const string Action = "action";
        public const string Success = "success";
        public const string Info = "info";
        public const string Failure = "failure";
    }
}
