﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Heartbeat.Core.Helpers;
using Newtonsoft.Json;

namespace Heartbeat.Core.Notifications
{
    public class NotificationServer
    {
        private static NotificationServer _instance;
        private static int _port;

        public static NotificationServer Instance => _instance ?? (_instance = new NotificationServer());

        private TcpListener _listener;
        private bool _started;

        private List<TcpClient> Clients { get; } = new List<TcpClient>();

        public static void Instantiate(int port)
        {
            _port = port;
        }

        public void Start()
        {
            _listener = new TcpListener(IPAddress.Any, _port);

            var thread = new Thread(ListenForClients);
            thread.Start();
            _started = true;
        }

        public void Stop()
        {
            try
            {
                _started = false;
                Clients.ForEach(x =>
                {
                    x.Close();
                    x.Dispose();
                });

                _listener.Stop();
            }
            catch (Exception e) { }
        }

        private void SendImmediate(Message message, TcpClient client)
        {
            var clientStream = client.GetStream();
            var binaryWriter = new BinaryWriter(clientStream);
            var json = JsonConvert.SerializeObject(message);
            binaryWriter.Write(json);
        }

        public void SendImmediateToAll(Exception ex)
        {
            var text = ex.Message + Environment.NewLine + ex.StackTrace;
            var message = Message.Create(text, MessageType.Error);
            SendImmediateToAll(message);
            WindowsEventLogger.Log(message);
        }

        public void SendImmediateToAll(Message message)
        {
            lock (Clients)
            {
                WindowsEventLogger.Log(message);
                foreach (var client in Clients)
                    SendImmediate(message, client);
            }
        }

        public void SendImmediateToAll(string data)
        {
            var message = Message.Create(data, "default");

            lock (Clients)
            {
                WindowsEventLogger.Log(message);
                foreach (var client in Clients)
                    SendImmediate(message, client);
            }
        }

        private void ListenForClients()
        {
            try
            {
                _listener.Start();
            }
            catch (Exception e) { }


            while (_started)
            {
                try
                {
                    var client = _listener.AcceptTcpClient();

                    Clients.Add(client);

                    Thread.Sleep(15);
                }
                catch (Exception e) { }
            }
        }
    }
}