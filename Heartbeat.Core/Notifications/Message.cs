﻿namespace Heartbeat.Core.Notifications
{
    public class Message
    {
        public string Text { get; set; }

        public string Type { get; set; }

        public static Message Create(string text, string type)
        {
            return new Message
            {
                Text = text,
                Type = type
            };
        }
    }
}