﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Timer = System.Timers.Timer;

namespace Heartbeat.Core.Services
{
    internal sealed class ExtScreenMonitoring : IStartableInstance
    {
        //Default launched events, no change on this part because of the functions
        public event EventHandler<HBEventArguments> Trigger;

        private readonly HBEventArguments _args = new HBEventArguments();

        private static TcpClient _client;

        //Screen status, null: unknown, 0: invalid,otherwise input name (HDMI1)
        private static string _screenStatus = "null";

        //Input status, null: unknown, 0: invalid,otherwise state name (on)
        private static string _inputStatus = "null";

        private static JToken _screenDriver;
        private static JToken _screen;
        private static bool _inError;

        private readonly List<string> _errors = new List<string>();

        private static Timer _timer;

        private static readonly int _interval = 10000;

        public ExtScreenMonitoring(JToken screen)
        {
            _screen = screen;
        }

        public void Start()
        {
            if (File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/drivers.json")))
            {
                try
                {
                    _screenDriver = JsonConvert.DeserializeObject<JToken>(@File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/drivers.json")));
                    _screenDriver = _screenDriver[_screen["driver"].ToString()];

                    _timer = new Timer(_interval);
                    _timer.Elapsed += MonitorTimer;
                    _timer.Start();
                }
                catch (Exception ex)
                {
                    NotificationServer.Instance.SendImmediateToAll(Message.Create("Screen driver loaded but no reference for " + _screen["driver"], MessageType.Error));
                    NotificationServer.Instance.SendImmediateToAll(ex);
                }
            }
            else
            {
                _args.message = "Cannot load screen driver (config/screen.json)";
                _args.type = MessageType.Error;
                Changed(_args);
            }
        }

        public void Stop()
        {
            _timer.Stop();
            _timer.Dispose();
        }

        private void MonitorTimer(object source, ElapsedEventArgs e)
        {
            _timer.Stop();
            try
            {
                if (screen_on(true))
                {
                    Thread.Sleep(1000);
                    _inError = false;
                    _inError = !input_valid(true);
                }
                else
                {
                    _inError = true;
                }
            }
            catch (Exception ex)
            {
                if (_screenStatus != "null")
                {
                    _screenStatus = "null";
                    _args.message = "No tcp/ip connection to the screen";
                    _args.type = MessageType.Error;
                    // changed(args);
                }
            }

            _timer.Start();
        }

        public bool IsOk()
        {
            return !_inError;
        }

        private bool input_valid(bool action)
        {
            var input = Send(_screenDriver["status"]["input"]["command"].ToString());

            try
            {
                if (input == _screenDriver["status"]["input"]["states"][_screen["states"]["input"].ToString()].ToString())
                {

                    if (_inputStatus != _screen["states"]["input"].ToString())
                    {
                        _inputStatus = _screen["states"]["input"].ToString();
                        _args.message = ($"[SCREEN] Input {_screen["states"]["input"]} is valid");
                        _args.type = MessageType.Success;
                        Changed(_args);
                    }
                    return true;
                }

                if (_inputStatus != "0")
                {
                    _errors.Add("input");
                    _inputStatus = "0";
                    _args.message = ($"Input  {_screen["states"]["input"]} invalid");

                    _args.type = MessageType.Error;
                    Changed(_args);

                    if (action)
                    {
                        _args.message = ($"Switching to {_screen["states"]["input"]}");
                        _args.type = MessageType.Action;
                        Changed(_args);
                        Send(_screenDriver["control"]["input"][_screen["states"]["input"].ToString()].ToString());
                    }
                }

                return false;

            }
            catch (Exception e)
            {
                if (_inputStatus != "null")
                {
                    _inputStatus = "null";
                    _args.type = MessageType.Error;
                    _errors.Add("input");
                    _args.message = ("Cannot read screen status");
                    Changed(_args);
                }
                return false;
            }
        }

        private bool screen_on(bool action)
        {
            var powerState = Send(_screenDriver["status"]["power"]["command"].ToString());

            try
            {
                if (powerState == _screenDriver["status"]["power"]["states"][_screen["states"]["power"].ToString()].ToString())
                {
                    if (_screenStatus != _screen["states"]["power"].ToString())
                    {
                        _screenStatus = _screen["states"]["power"].ToString();
                        _args.message = ($"Screen {_screen["states"]["power"]}");
                        _args.type = MessageType.Success;
                        Changed(_args);
                    }
                    return true;
                }

                if (_screenStatus != "0")
                {
                    _screenStatus = "0";
                    _args.message = ($"Screen is not {_screen["states"]["power"]}");
                    _errors.Add("input");
                    _args.type = MessageType.Error;
                    Changed(_args);

                    if (action)
                    {
                        _args.message = ($"[SCREEN] Turning screen {_screen["states"]["power"]}");
                        _args.type = MessageType.Action;
                        Changed(_args);
                        Send(_screenDriver["control"]["power"][_screen["states"]["power"].ToString()].ToString());
                    }

                }
                return false;
            }
            catch (Exception e)
            {
                if (_screenStatus != "null")
                {
                    _screenStatus = "null";
                    _args.message = "Cannot read screen status";
                    _args.type = MessageType.Error;
                    _errors.Add("input");
                    // changed(args);
                }
                return false;
            }
        }

        private string Send(string hexString)
        {
            _client = new TcpClient(_screen["ip"].ToString(), int.Parse(_screen["port"].ToString()));
            var nwStream = _client.GetStream();

            nwStream.WriteTimeout = 1000;
            nwStream.ReadTimeout = 1000;
            var bytesToSend = HexString2Bytes(hexString);

            nwStream.Write(bytesToSend, 0, bytesToSend.Length);

            var bytes = new byte[_client.ReceiveBufferSize];
            var bytesRead = nwStream.Read(bytes, 0, (int)_client.ReceiveBufferSize);
            var returnData = BitConverter.ToString(bytes, 0, bytesRead).Replace("-", "");

            _client.Close();
            _client.Dispose();

            return returnData;
        }


        private void Changed(HBEventArguments args)
        {
            Trigger?.Invoke(this, args);
        }

        private static byte[] HexString2Bytes(string hexString)
        {
            if (hexString == null)
                return null;

            var len = hexString.Length;
            if (len % 2 == 1)
                return null;

            var lenHalf = len / 2;
            var bs = new byte[lenHalf];

            try
            {
                //convert the hex string to bytes
                for (var i = 0; i != lenHalf; i++)
                {
                    bs[i] = (byte)int.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception ex) { }

            return bs;
        }
    }
}