﻿namespace Heartbeat.Core.Services
{
    public interface IStartableInstance
    {
        void Start();

        void Stop();
    }
}