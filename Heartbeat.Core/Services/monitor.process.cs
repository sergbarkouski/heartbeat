﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Newtonsoft.Json.Linq;

namespace Heartbeat.Core.Services
{
    internal sealed class MonitoringProcess : IStartableInstance
    {
        //Default launched events, no change on this part because of the functions
        public event EventHandler<HBEventArguments> Trigger;

        //Load actions object
        private readonly monitoring_actions _ma = new monitoring_actions();
        private readonly HBEventArguments _args = new HBEventArguments();
        private static readonly StatusList Status = new StatusList();

        private static string _name;
        private static string _process;
        private static readonly int Interval = 2000;
        private readonly JToken _data;
        private readonly CancellationTokenSource _cancellationTokenSource;

        public MonitoringProcess(string name, string process, JToken data)
        {
            _process = process;
            _name = name;
            _data = data;
            _cancellationTokenSource = new CancellationTokenSource();

            _ma.ACTION += HeartBeatActionEvent;
        }

        public void Start()
        {
            try
            {
                if (Process.GetProcessesByName(_process).Length > 0)
                {
                    if (!Status.isActive(_name, _name))
                    {
                        _args.message = $"{_name} started";
                        _args.type = MessageType.Info;
                        Changed(_args);

                        Status.AddStatus(_name, _name, true);
                    }

                    _ma.ok("process", _process);
                }
                else
                {

                    if (Status.isActive(_name, _name) || !Status.exists(_name, _name))
                    {
                        _args.message = $"{_name} not running";
                        _args.type = MessageType.Error;
                        Changed(_args);

                        Status.AddStatus(_name, _name, false);
                    }

                    _ma.error("process", _process, _data);
                }

                Task.Delay(Interval, _cancellationTokenSource.Token).ContinueWith(t => Start(), _cancellationTokenSource.Token);
            }
            catch (TaskCanceledException) { }
            catch (Exception ex)
            {
                NotificationServer.Instance.SendImmediateToAll(ex);
            }
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }

        //isOK is a default event function to check the status of the monitoring process
        public bool IsOk()
        {
            return Process.GetProcessesByName(_process).Length > 0;
        }

        private void Changed(HBEventArguments args)
        {
            Trigger?.Invoke(this, args);
        }

        private void HeartBeatActionEvent(object sender, HBEventArguments args)
        {
            Changed(args);
        }
    }
}