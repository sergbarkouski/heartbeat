﻿using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Heartbeat.Core.Services
{
    internal sealed class SystemInfo
    {
        public static JToken app_settings = JsonConvert.DeserializeObject<JToken>(@File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/settings.json")));

        public SystemInfo()
        {
            var deviceInfo = GetDeviceInfo();

            NotificationServer.Instance.SendImmediateToAll(deviceInfo.MachineName);
            NotificationServer.Instance.SendImmediateToAll(deviceInfo.Ip);
            NotificationServer.Instance.SendImmediateToAll(deviceInfo.SerialNumber);
            NotificationServer.Instance.SendImmediateToAll(deviceInfo.AssemblyName);
            NotificationServer.Instance.SendImmediateToAll(deviceInfo.AssemblyVersion);
            NotificationServer.Instance.SendImmediateToAll(deviceInfo.Bios);

            try
            {
                foreach (JToken process in app_settings["monitoring"]["processes"])
                {

                    Console.WriteLine(process["name"] + "/" + FileVersionInfo.GetVersionInfo(process["location"].ToString()).ProductVersion);
                    NotificationServer.Instance.SendImmediateToAll(process["name"] + "/" + FileVersionInfo.GetVersionInfo(process["location"].ToString()).ProductVersion);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                NotificationServer.Instance.SendImmediateToAll(e.Message);
            }

            NotificationServer.Instance.SendImmediateToAll(deviceInfo.OsName + "/" + deviceInfo.OsBuild + "." + deviceInfo.OsVersion);

            using (var searcher = new ManagementObjectSearcher($"SELECT * FROM Win32_SoundDevice"))
            {
                foreach (var device in searcher.Get())
                {
                    Console.WriteLine(device["name"].ToString());
                    NotificationServer.Instance.SendImmediateToAll(device["name"].ToString());
                }
            }
        }

        public static string GetSerialNumber()
        {
            var searcher = new ManagementObjectSearcher("select * from Win32_Processor");
            var collectedInfo = ""; // here we will put the informa

            searcher.Query = new ObjectQuery("select * from Win32_BIOS");
            foreach (var share in searcher.Get())
            {
                //then, the serial number of BIOS
                collectedInfo += share.GetPropertyValue("SerialNumber").ToString();
            }
            return collectedInfo;
        }

        public static string GetLocalIpAddress()
        {
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                var endPoint = socket.LocalEndPoint as IPEndPoint;
                return endPoint.Address.ToString();
            }
        }

        public static DeviceInfo GetDeviceInfo()
        {
            var assemblyInfo = Assembly.GetEntryAssembly()?.GetName();

            return new DeviceInfo
            {
                MachineName = Environment.MachineName,
                Ip = GetLocalIpAddress(),
                SerialNumber = GetSerialNumber(),
                AssemblyName = assemblyInfo?.Name,
                AssemblyVersion = assemblyInfo?.Version.ToString(),
                Bios = GetBiosVersion(),
                OsName = GetOsName(),
                OsVersion = GetOsVersion(),
                OsBuild = GetOsBuild()
            };
        }

        private static string GetBiosVersion()
        {
            return
            (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\BIOS", "BIOSVendor", null) +
            "/" +
            (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\BIOS", "BIOSVersion", null);
        }

        private static string GetOsName()
        {
            return (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", null);
        }

        private static string GetOsVersion()
        {
            return (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ReleaseId", null);
        }

        private static string GetOsBuild()
        {
            return (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentBuild", null);
        }
    }
}
