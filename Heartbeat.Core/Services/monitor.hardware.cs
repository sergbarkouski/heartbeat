﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Threading;
using System.Threading.Tasks;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Newtonsoft.Json.Linq;

namespace Heartbeat.Core.Services
{
    internal sealed class MonitoringHardware : IStartableInstance
    {
        public event EventHandler<HBEventArguments> Trigger;

        private static readonly StatusList Status = new StatusList();
        private static readonly HBEventArguments Args = new HBEventArguments();
        private static JToken _hardware;
        private static bool _foundDevice;
        private static readonly int Interval = 1000;
        private static bool _isOk = true;

        //Load actions object
        private readonly monitoring_actions _ma = new monitoring_actions();
        private readonly List<string> _errors = new List<string>();
        private readonly CancellationTokenSource _cancellationTokenSource;

        public MonitoringHardware(JToken hardware)
        {
            _cancellationTokenSource = new CancellationTokenSource();

            _ma.ACTION += HeartBeatActionEvent;
            _hardware = hardware;
        }

        public bool IsOk()
        {
            return _isOk;
        }

        public void Start()
        {
            _errors.Clear();
            try
            {
                foreach (var hwDevice in _hardware)
                {
                    _foundDevice = false;

                    using (var searcher = new ManagementObjectSearcher(
                        $"SELECT * FROM Win32_PnPEntity WHERE (Manufacturer = '{hwDevice["manufacturer"]}' AND Name = '{hwDevice["name"]}')")
                    )
                    {
                        foreach (var unused in searcher.Get())
                        {
                            _foundDevice = true;
                            if (!Status.isActive(hwDevice["name"].ToString(), hwDevice["manufacturer"].ToString()))
                            {
                                Status.AddStatus(hwDevice["name"].ToString(), hwDevice["manufacturer"].ToString(), true);
                                Args.message = ($"Device {hwDevice["manufacturer"]} {hwDevice["name"]} connected! ");
                                Args.type = MessageType.Info;
                                Changed(Args);
                            }
                        }
                    }

                    if (!_foundDevice)
                    {

                        if (Status.isActive(hwDevice["name"].ToString(), hwDevice["manufacturer"].ToString()) ||
                            !Status.exists(hwDevice["name"].ToString(), hwDevice["manufacturer"].ToString()))
                        {

                            Status.AddStatus(hwDevice["name"].ToString(), hwDevice["manufacturer"].ToString(), false);
                            Args.message = ($"Device {hwDevice["manufacturer"]} {hwDevice["name"]} disconnected! ");
                            Args.type = MessageType.Error;
                            Changed(Args);
                        }

                        _errors.Add(hwDevice["name"] + "_" + hwDevice["manufacturer"]);
                        _ma.error("hardware", hwDevice["name"] + "_" + hwDevice["manufacturer"], _hardware);
                    }
                    else
                    {
                        _ma.ok("hardware", hwDevice["name"] + "_" + hwDevice["manufacturer"]);
                    }
                }

                Task.Delay(Interval, _cancellationTokenSource.Token).ContinueWith(t => Start(), _cancellationTokenSource.Token);

                _isOk = _errors.Count <= 0;
            }
            catch (TaskCanceledException) { }
            catch (Exception ex)
            {
                NotificationServer.Instance.SendImmediateToAll(ex);
            }
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }

        private void Changed(HBEventArguments args)
        {
            Trigger?.Invoke(this, args);
        }

        private void HeartBeatActionEvent(object sender, HBEventArguments args)
        {
            Changed(args);
        }
    }
}