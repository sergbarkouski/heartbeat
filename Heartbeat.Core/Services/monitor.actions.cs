﻿using System;
using System.Linq;
using Heartbeat.Core.Extensions;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Newtonsoft.Json.Linq;

namespace Heartbeat.Core.Services
{
    internal class monitoring_actions
    {
        private HeartbeatDatabase db;

        public event EventHandler<HBEventArguments> ACTION;
        private static HBEventArguments args = new HBEventArguments();

        public monitoring_actions()
        {
            db = new HeartbeatDatabase();
        }

        public void ok(string type, string resource)
        {
            db.delete($"{type}-{resource}");
        }

        public void error(string type, string resource, JToken data)
        {
            //Get latest action
            var last_action = db.getLastAction($"{type}-{resource}");
            var last_status = db.getLastStatus($"{type}-{resource}");
            //Loop through actions
            if (data[0]["actions"].Any() && last_status != "pending")
            {

                bool action_found = false;
                foreach (JToken action in data[0]["actions"])
                {
                    //Find last action or move on
                    if (last_action == "" || action_found)
                    {
                        db.status_update($"{type}-{resource}", action["action"].ToString(), "pending");

                        //Action is found, do the action
                        if (action["action"].ToString() == "reboot")
                        {
                            args.message = "Reboot is pending... ";
                            args.type = MessageType.Action;
                            OnAction(args);
                        }
                        else if (action["action"].ToString() == "reload")
                        {
                            args.message = ($"Application {data[0]["name"]} restart is pending... ");
                            args.type = MessageType.Action;
                            OnAction(args);


                            try
                            {
                                ProcessExtensions.StartProcessAsCurrentUser(@data[0]["location"].ToString(), data[0]["arguments"].ToString());
                            }
                            catch (Exception e)
                            {
                            }

                            db.status_update($"{type}-{resource}", action["action"].ToString(), "done");
                        }

                        break;
                    }

                    if (action["action"].ToString() == last_action)
                    {
                        action_found = true;
                    }
                }
            }
        }

        protected virtual void OnAction(HBEventArguments args)
        {
            ACTION?.Invoke(this, args);
        }
    }
}
