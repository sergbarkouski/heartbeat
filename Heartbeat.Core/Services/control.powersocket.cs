﻿using System;
using System.IO;
using System.Net;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Notifications;

namespace Heartbeat.Core.Services
{
    internal sealed class control_powersocket
    {
        public bool startSwitch(string mac, string password, string socket, string command)
        {
            try
            {
                if (string.IsNullOrEmpty(mac))
                {
                    return false;
                }

                var ip = IpMacMappingHelper.FindIpFromMacAddress(mac);

                if (string.IsNullOrEmpty(ip))
                {
                    NetworkHelper.Scan();
                    ip = IpMacMappingHelper.FindIpFromMacAddress(mac);
                }

                if (string.IsNullOrEmpty(ip))
                {
                    return false;
                }

                {
                    var url = "http://" + ip + "/login.html";
                    var postData = "pw=" + Uri.EscapeDataString(password);
                    var request = RequestHelper.CreatePostRequest(url, postData);
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                }

                {
                    var url = "http://" + ip;
                    var postData = "ctl" + socket + "=" + Uri.EscapeDataString(command);
                    var request = RequestHelper.CreatePostRequest(url, postData);
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                }

                return true;
            }
            catch (Exception e)
            {
                NotificationServer.Instance.SendImmediateToAll(e.Message);
                NotificationServer.Instance.SendImmediateToAll(e.Source);
                NotificationServer.Instance.SendImmediateToAll(e.StackTrace);
                return false;
            }
        }
    }
}