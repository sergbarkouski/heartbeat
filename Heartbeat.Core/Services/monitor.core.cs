using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Heartbeat.Core.Constants;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using JsonFlatFileDataStore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Heartbeat.Core.Services
{
    public class Monitoring : IStartableInstance
    {
        private static JToken _appSettings;

        private MonitoringProcess _mp;
        private MonitoringHardware _mhw;
        private ExtScreenMonitoring _sm;
        private WebsocketServer _websocketServer;

        private readonly HBEventArguments Args = new HBEventArguments();
        private readonly DataStore DataStore = new DataStore(@Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/hb.db.json"), reloadBeforeGetCollection: true);
        private IDocumentCollection<MonitoringStatus> _col;

        private readonly CancellationTokenSource _cancellationTokenSource;

        public Monitoring()
        {
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public void Start()
        {
            try
            {
                CheckAllGood();

                var version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                NotificationServer.Instance.SendImmediateToAll($"Heartbeat v{version}");

                if (!SettingsLoaded())
                    return;

                MonitoringInErrorStatus(false);

                try
                {
                    var id = _appSettings["general"]["id"].ToString();
                    if (string.IsNullOrWhiteSpace(id))
                    {
                        id = SystemInfo.GetSerialNumber();
                    }

                    var key = _appSettings["general"]["key"]?.ToString();
                    if (string.IsNullOrWhiteSpace(key))
                        key = "CLIENTKEY001";

                    _websocketServer = new WebsocketServer($"wss://{_appSettings["general"]["heartbeat"]["server"]}:{_appSettings["general"]["heartbeat"]["port"]}", id, key);
                    _websocketServer.WSSMessage += WssMessage;
                    _websocketServer.Start();
                }
                catch (Exception e)
                {
                    NotificationServer.Instance.SendImmediateToAll(Message.Create("Cannot start HeartBeat connection, please check if server is running and settings correct.", MessageType.Error));
                }

                _mhw = new MonitoringHardware(_appSettings["monitoring"]["hardware"]);
                _mhw.Trigger += HeartBeatEvent;
                _mhw.Start();

                foreach (var process in _appSettings["monitoring"]["processes"])
                {
                    _mp = new MonitoringProcess(process["name"].ToString(), process["process"].ToString(), _appSettings["monitoring"]["processes"]);
                    _mp.Trigger += HeartBeatEvent;
                    _mp.Start();
                }

                foreach (var screen in _appSettings["monitoring"]["screens"])
                {
                    _sm = new ExtScreenMonitoring(screen);
                    _sm.Trigger += HeartBeatEvent;
                    _sm.Start();
                }
            }
            catch (TaskCanceledException) { }
            catch (Exception e)
            {
                NotificationServer.Instance.SendImmediateToAll(e.Message);
            }
        }

        public void Stop()
        {
            _mhw.Stop();
            _mp.Stop();
            _sm.Stop();
            _websocketServer.Stop();

            _cancellationTokenSource.Cancel();
        }

        private static bool SettingsLoaded()
        {
            if (File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/settings.json")))
            {
                try
                {
                    _appSettings = JsonConvert.DeserializeObject<JToken>(File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config/settings.json")));

                    if (_appSettings["general"].ToString() == "")
                    {
                        NotificationServer.Instance.SendImmediateToAll(Message.Create("Settings invalid, please check format", MessageType.Error));
                        return false;
                    }

                    NotificationServer.Instance.SendImmediateToAll(Message.Create("Settings loaded", MessageType.Info));
                }
                catch (Exception e)
                {
                    NotificationServer.Instance.SendImmediateToAll(Message.Create("Settings invalid, please check format", MessageType.Error));
                    return false;
                }

                return true;
            }

            NotificationServer.Instance.SendImmediateToAll(Message.Create("Cannot load settings (config/settings.json)", MessageType.Error));
            return false;
        }

        private void HeartBeatEvent(object sender, HBEventArguments args)
        {
            SendToServer(args);
        }

        private void WssMessage(object sender, HBEventArguments args)
        {
            try
            {
                var response = JsonConvert.DeserializeObject<JToken>(args.message);

                switch (response.SelectToken("action")?.ToString())
                {
                    case HeartbeatActions.Power: HandlePowerAction(response, args); return;
                    case HeartbeatActions.Update: HandleUpdateActionCommand(); return;
                    case HeartbeatActions.Settings: HandleSettingsActionCommand(); return;
                    default: return;
                }
            }
            catch (Exception e)
            {
                NotificationServer.Instance.SendImmediateToAll(e.Message);
            }
        }

        private void HandleSettingsActionCommand()
        {
            var updateService = new UpdateService();
            var args = updateService.CheckForSettingsUpdates();
            SendToServer(args, false);

            Task.Delay(3000).ContinueWith(t =>
            {
                var startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false,
                    FileName = "powershell.exe",
                    Arguments = $"Restart-Service \"{HeartbeatConstants.HeartbeatServiceName}\""
                };

                Process.Start(startInfo);
            });
        }

        private void HandleUpdateActionCommand()
        {
            var updateService = new UpdateService();
            var args = updateService.CheckForAppUpdates();

            SendToServer(args, false);
        }

        private void HandlePowerAction(JToken response, HBEventArguments args)
        {
            var cps = new control_powersocket();

            args.type = MessageType.Info;
            args.message = $"Power switch action on {response["mac"]} started";
            SendToServer(args);

            if (cps.startSwitch(response["mac"].ToString(), _appSettings["general"]["powersocket"]["password"].ToString(), response["socket"].ToString(), response["state"].ToString()))
            {
                args.type = MessageType.Success;
                args.message = $"Power switch action on {response["mac"]} success";
                SendToServer(args);
            }
            else
            {
                args.type = MessageType.Error;
                args.message = $"Power switch action on {response["mac"]} failed";
                SendToServer(args);
            }
        }

        private void SendToServer(HBEventArguments arg, bool showInVisualizationApp = true)
        {
            Console.BackgroundColor = ConsoleHelper.ChooseColor(arg.type);
            arg.date = DateTime.Now;

            var id = _appSettings["general"]["id"].ToString();
            if (string.IsNullOrWhiteSpace(id))
            {
                id = SystemInfo.GetSerialNumber();
            }

            var key = _appSettings["general"]["key"]?.ToString();
            if (string.IsNullOrWhiteSpace(key))
                key = "CLIENTKEY001";

            var payload = Payload.Create(arg.type, arg.date, arg.message);
            _websocketServer.Send(WebSocketMessage.Create(id, key, payload).ToJson());

            if (!showInVisualizationApp)
                return;

            var message = Message.Create($"{arg.date} > {arg.message}", arg.type);
            NotificationServer.Instance.SendImmediateToAll(message);
        }

        private void CheckAllGood()
        {
            try
            {
                if (_mhw != null && _mp != null && _sm != null && _mhw.IsOk() && _mp.IsOk() && _sm.IsOk())
                {
                    if (MonitoringInError())
                    {
                        MonitoringInErrorStatus(false);
                        Args.type = MessageType.Success;
                        Args.message = "System running without errors";
                        SendToServer(Args);
                    }
                }
                else
                {
                    if (!MonitoringInError())
                    {
                        MonitoringInErrorStatus(true);
                        Args.type = MessageType.Failure;
                        Args.message = "System is running in error mode";
                        SendToServer(Args);
                    }
                }

                Task.Delay(3000, _cancellationTokenSource.Token).ContinueWith(t => CheckAllGood(), _cancellationTokenSource.Token);
            }
            catch (TaskCanceledException) { }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                NotificationServer.Instance.SendImmediateToAll(Message.Create(e.Message, MessageType.Error));
            }
        }

        private bool MonitoringInError()
        {
            try
            {
                _col = DataStore.GetCollection<MonitoringStatus>();
                var result = _col.Find(e => e.type == "monitoring").ToList();

                return !result.Any() || result.First().system_in_error;
            }
            catch (Exception e)
            {
                return true;
            }
        }

        private void MonitoringInErrorStatus(bool inError)
        {
            _col = DataStore.GetCollection<MonitoringStatus>();
            var action = new MonitoringStatus
            {
                type = "monitoring",
                system_in_error = inError,
                updated = DateTime.Now
            };

            var results = _col.Find(e => e.type == "monitoring").ToList();
            if (results.Any())
            {
                _col.UpdateOne(e => e.type == "monitoring", action);
            }
            else
            {
                _col.InsertOne(action);
            }
        }
    }
}
