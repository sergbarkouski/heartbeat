﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Heartbeat.Core.Constants;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Models;
using Heartbeat.Core.Notifications;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Heartbeat.Core.Services
{
    public class UpdateService
    {
        public HBEventArguments CheckForSettingsUpdates()
        {
            var (newSettingsVersion, settings) = GetNewSettingsVersion();
            var currentSettingsVersion = GetCurrentSettingsVersion();

            if (currentSettingsVersion == null || currentSettingsVersion != newSettingsVersion)
            {
                return UpdateSettings(settings.ToString());
            }

            return new HBEventArguments { message = "Settings are up-to-date", type = MessageType.Info };
        }

        public HBEventArguments CheckForAppUpdates()
        {
            var (newMajorAppVersion, newMinorAppVersion, newAppVersionUrl) = GetNewAppVersionInfo();
            var currentApplicationVersion = Assembly.GetExecutingAssembly().GetName().Version;

            if (currentApplicationVersion.Major < newMajorAppVersion ||
                currentApplicationVersion.Major == newMajorAppVersion && currentApplicationVersion.Minor < newMinorAppVersion)
            {
                return UpdateApplication(newAppVersionUrl);
            }

            return new HBEventArguments { message = "Application is up-to-date", type = MessageType.Info };
        }

        private static string GetCurrentSettingsVersion()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config", "settings.json");

            if (!File.Exists(path))
                return null;

            try
            {
                var raw = File.ReadAllText(path);
                var settings = JsonConvert.DeserializeObject<JToken>(raw);

                return settings["general"]["version"]?.ToString();
            }
            catch (Exception)
            {
                throw new InvalidOperationException("Settings invalid, please check format");
            }
        }

        private static (string, JToken) GetNewSettingsVersion()
        {
            try
            {
                var request = RequestHelper.CreateGetRequest("http://heartbeat.heuvelman.nl/external/settings/");
                var response = request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var result = JsonConvert.DeserializeObject<JToken>(responseString);

                return (result["general"]["version"].ToString(), result);
            }
            catch (Exception)
            {
                Task.Delay(3000).Wait();
                return GetNewSettingsVersion();
            }
        }

        private static (int, int, string) GetNewAppVersionInfo()
        {
            try
            {
                var request = RequestHelper.CreateGetRequest("http://heartbeat.heuvelman.nl/external/update/");
                var response = request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var result = JsonConvert.DeserializeObject<JToken>(responseString);

                return ((int)result["latest_version"]["major"],
                    (int)result["latest_version"]["minor"],
                    result["latest_version"]["url"]?.ToString());
            }
            catch (Exception)
            {
                Task.Delay(3000).Wait();
                return GetNewAppVersionInfo();
            }
        }

        private static HBEventArguments UpdateSettings(string settings)
        {
            Message msg;
            try
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config", "settings.json");
                File.WriteAllText(path, settings);

                msg = Message.Create("Settings were updated successfully.", MessageType.Success);
                NotificationServer.Instance.SendImmediateToAll(msg);
            }
            catch (Exception e)
            {
                msg = Message.Create($"Settings update failed: {e.Message}", MessageType.Error);
                NotificationServer.Instance.SendImmediateToAll(msg);
            }

            return new HBEventArguments { message = msg.Text, type = msg.Type };
        }

        #region Update application

        public HBEventArguments UpdateApplication(string url)
        {
            var filename = new Uri(url).Segments.Last();
            var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, HeartbeatConstants.UpdateFolder);
            var path = Path.Combine(directory, filename);

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            var (isDownloaded, args) = DownloadNewMsi(url, path, filename);
            var fileExists = File.Exists(path);

            if (!isDownloaded)
                return new HBEventArguments { message = "File was not downloaded", type = MessageType.Error, date = DateTime.UtcNow };

            if (!fileExists)
                return new HBEventArguments { message = "File does not exist in upload directory", type = MessageType.Error, date = DateTime.UtcNow };

            Task.Delay(2000).ContinueWith(t => RunInstallation(directory, filename));
            return args;
        }

        private static (bool, HBEventArguments) DownloadNewMsi(string url, string path, string filename)
        {
            using (var webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, path);

                    var msg = Message.Create($"New version {filename} was downloaded successfully.", MessageType.Success);
                    NotificationServer.Instance.SendImmediateToAll(msg);

                    Task.Delay(2000).Wait();
                    return (true, new HBEventArguments { message = msg.Text, type = msg.Type, date = DateTime.UtcNow });
                }
                catch (Exception)
                {
                    var msg = Message.Create("New version failed to download.", MessageType.Error);
                    NotificationServer.Instance.SendImmediateToAll(msg);

                    return (false, new HBEventArguments { message = msg.Text, type = msg.Type, date = DateTime.UtcNow });
                }
            }
        }

        private static void RunInstallation(string directory, string filename)
        {
            try
            {
                var startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false,
                    FileName = "msiexec",
                    Arguments = $"/i \"{Path.Combine(directory, filename)}\" /l*v \"{Path.Combine(directory, "msilog.txt")}\" /qn"
                };

                Process.Start(startInfo);
            }
            catch (Exception ex)
            {
                NotificationServer.Instance.SendImmediateToAll(ex);
            }
        }

        #endregion
    }
}