﻿using System.Management;
using System.Threading.Tasks;
using Heartbeat.Core.Helpers;

namespace Heartbeat.Core.Services
{
    public class NetworkService
    {
        public bool IsOnline(int attempts)
        {
            return NetworkHelper.CheckForInternetConnection(attempts);
        }

        public void RestartNetworkInterfaces()
        {
            SwitchNetworkInterfaces(false);
            Task.Delay(1000).Wait();
            SwitchNetworkInterfaces(true);
            Task.Delay(3000).Wait();
        }

        private static void SwitchNetworkInterfaces(bool enable)
        {
            var query = new ObjectQuery("SELECT * FROM Win32_NetworkAdapter");

            using (var searcher = new ManagementObjectSearcher(query))
            {
                var queryCollection = searcher.Get();
                foreach (ManagementObject managementObject in queryCollection)
                {
                    if (!bool.TryParse(managementObject["NetEnabled"]?.ToString(), out var isEnabled))
                        continue;

                    if (enable)
                    {
                        if (!isEnabled)
                            managementObject.InvokeMethod("Enable", null);
                    }
                    else
                    {
                        if (isEnabled)
                            managementObject.InvokeMethod("Disable", null);
                    }
                }
            }
        }
    }
}
