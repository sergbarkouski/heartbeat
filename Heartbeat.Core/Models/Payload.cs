﻿using System;
using Newtonsoft.Json;

namespace Heartbeat.Core.Models
{
    public class Payload
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("date")]
        public DateTime? Date { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        public static Payload Create(string type, DateTime? date, string message)
        {
            return new Payload
            {
                Type = type,
                Date = date,
                Message = message
            };
        }
    }
}