﻿using Heartbeat.Core.Constants;
using Newtonsoft.Json;

namespace Heartbeat.Core.Models
{
    public class WebSocketMessage
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("sendToServer")]
        public Payload Payload { get; set; }

        [JsonProperty("device")]
        public DeviceInfo DeviceInfo { get; set; }

        public static WebSocketMessage Create(string id, string key, Payload payload)
        {
            return new WebSocketMessage
            {
                Id = id,
                Key = key,
                Payload = payload
            };
        }

        public static WebSocketMessage Create(string id, string key, DeviceInfo deviceInfo)
        {
            return new WebSocketMessage
            {
                Id = id,
                Key = key,
                DeviceInfo = deviceInfo
            };
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, JsonSerializerSettingsConstants.DefaultSettings);
        }
    }
}