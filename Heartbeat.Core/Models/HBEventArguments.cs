﻿using System;

namespace Heartbeat.Core.Models
{
    public class HBEventArguments : EventArgs
    {
        public string type { get; set; }

        public string status { get; set; }

        public int code { get; set; }

        public string message { get; set; }

        public DateTime date { get; set; }
    }
}