﻿using System;

namespace Heartbeat.Core.Models
{
    public class MonitoringStatus
    {
        public string type { get; set; }

        public bool system_in_error { get; set; }

        public DateTime updated { get; set; }
    }
}
