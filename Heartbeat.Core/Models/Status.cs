﻿namespace Heartbeat.Core.Models
{
    public class Status
    {
        public string name { get; set; }

        public string manufacturer { get; set; }

        public bool active { get; set; }
    }
}
