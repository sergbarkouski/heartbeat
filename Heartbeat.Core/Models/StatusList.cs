﻿using System;
using System.Collections.Generic;
using Heartbeat.Core.Notifications;

namespace Heartbeat.Core.Models
{
    public class StatusList
    {
        private readonly List<Status> _status = new List<Status>();

        public bool AddStatus(string _name, string _manufacturer, bool _active)
        {
            if (exists(_name, _manufacturer))
            {
                delete(_name, _manufacturer);
            }
            _status.Add(new Status { name = _name, manufacturer = _manufacturer, active = _active });
            return true;
        }

        public bool exists(string name, string manufacturer)
        {
            for (var i = 0; i < _status.Count; i++)
            {
                if (_status[i].name == name && _status[i].manufacturer == manufacturer)
                {
                    return true;
                }
            }
            return false;
        }

        public bool isActive(string name, string manufacturer)
        {
            for (var i = 0; i < _status.Count; i++)
            {
                if (_status[i].name == name && _status[i].manufacturer == manufacturer)
                {

                    return _status[i].active;


                }
            }

            return false;
        }

        public bool all()
        {

            for (var i = 0; i < _status.Count; i++)
            {
                Console.WriteLine($"Name: {_status[i].name} Mani: {_status[i].manufacturer} active: {_status[i].active}");
                NotificationServer.Instance.SendImmediateToAll($"Name: {_status[i].name} Mani: {_status[i].manufacturer} active: {_status[i].active}");
            }
            return true;
        }


        public bool delete(string name, string manufacturer)
        {
            for (var i = 0; i < _status.Count; i++)
            {
                if (_status[i].name == name && _status[i].manufacturer == manufacturer)
                {
                    _status.RemoveAt(i);
                }
            }
            return true;
        }
    }
}
