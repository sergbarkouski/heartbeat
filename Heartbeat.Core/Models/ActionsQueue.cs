﻿using System;

namespace Heartbeat.Core.Models
{
    public class ActionsQueue
    {
        public string queue_id { get; set; }

        public string last_action { get; set; }

        public string status { get; set; }

        public DateTime updated { get; set; }
    }
}
