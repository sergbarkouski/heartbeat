﻿using Newtonsoft.Json;

namespace Heartbeat.Core.Models
{
    public class DeviceInfo
    {
        [JsonProperty("name")]
        public string MachineName { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("serial")]
        public string SerialNumber { get; set; }

        [JsonProperty("software")]
        public string AssemblyName { get; set; }

        [JsonProperty("version")]
        public string AssemblyVersion { get; set; }

        [JsonProperty("bios")]
        public string Bios { get; set; }

        [JsonProperty("os_name")]
        public string OsName { get; set; }

        [JsonProperty("os_version")]
        public string OsVersion { get; set; }

        [JsonProperty("os_build")]
        public string OsBuild { get; set; }
    }
}