﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using Heartbeat.Core.Constants;
using Heartbeat.Core.Helpers;
using Heartbeat.Core.Notifications;

namespace Heartbeat.Visualization
{
    public class Program
    {
        private static Client _client;
        private static DateTime? _lastServiceRestartTime;

        private static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    if (_client != null && _client.IsConnected())
                        continue;

                    _client = InitializeClient();
                    _client.OnMessageReceived += OnMessageReceived;
                }
                catch (SocketException)
                {
                    RestartService();
                }
                catch (Exception ex)
                {
                    OnMessageReceived(Message.Create(ex.Message, MessageType.Error));
                }
            }
        }

        private static Client InitializeClient()
        {
            var client = new Client();
            while (!client.IsConnected())
            {
                try
                {
                    client.ConnectToServer("127.0.0.1", HeartbeatConstants.NotificationPort);
                    OnMessageReceived(Message.Create("Connected to Heartbeat.Service", MessageType.Success));
                }
                catch (SocketException ex)
                {
                    RestartService();
                }
                catch (Exception ex)
                {
                    OnMessageReceived(Message.Create(ex.Message, MessageType.Error));
                }
            }

            return client;
        }

        private static void RestartService()
        {
            if (_lastServiceRestartTime.HasValue && DateTime.UtcNow.AddSeconds(-15) < _lastServiceRestartTime)
                return;

            _lastServiceRestartTime = DateTime.UtcNow;

            var startInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                FileName = "powershell.exe",
                Arguments = $"Restart-Service \"{HeartbeatConstants.HeartbeatServiceName}\""
            };

            Process.Start(startInfo);
        }

        private static void OnMessageReceived(Message message)
        {
            Console.BackgroundColor = ConsoleHelper.ChooseColor(message.Type);
            if (Console.BackgroundColor == ConsoleColor.Black)
            {
                Console.WriteLine(message.Text);
                return;
            }

            var output = $"[{message.Type.ToUpper()}] {message.Text}";
            Console.WriteLine(output);
        }
    }
}